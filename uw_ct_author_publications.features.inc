<?php
/**
 * @file
 * uw_ct_author_publications.features.inc
 */

/**
 * Implements hook_views_api().
 */
function uw_ct_author_publications_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
